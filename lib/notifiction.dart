import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:socket/settings.dart';

class CustomNotification{

  static FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

  static notificationConfig({SelectNotificationCallback onCLickNofification}){
    flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
    var android = new AndroidInitializationSettings('@mipmap/ic_launcher');
    var iOS = new IOSInitializationSettings();
    var initSetttings = new InitializationSettings(android, iOS);
    flutterLocalNotificationsPlugin.initialize(initSetttings,
        onSelectNotification: onCLickNofification);
  }

  static showNotification(titulo, text) async{
    await flutterLocalNotificationsPlugin.show(
      0,
      titulo,
      text,
      NotificationDetails(_initAndroid(), _initIOS()),
      payload: 'Default_Sound',
    );
  }

  static _initIOS(){
    return new IOSNotificationDetails();
  }

  static _initAndroid(){
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
      Settings.notificationChannelId,
      Settings.notificationChannelName,
      'Especifcs',
      importance: Importance.High,
      //color: Colors
      //AndroidBitmap:
    );
    return androidPlatformChannelSpecifics;
  }
}