
class Settings {
  static const String apiUrl = 'https://api.sdbusiness.com.br:3000';
  static const String theme = 'Light';

  static const String backgroundChannelId =
      'com.br.sdredes.SdBUSINESS.background';

  static const String notificationChannelId =
      'com.br.sdredes.SdBUSINESS.notification';
  static const String notificationChannelName = 'SdBUSINESS_NAME';

  static const bool isDebug = false;
}
